<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    //
    public function login()
    {
        $credentials = request(['email', 'password']);
        if (!auth()->attempt($credentials)) {
            return response()->json([
                'message' => 'Invalid login credentials',
            ], 401);
        }

        $token = auth()->user()->createToken('authToken')->accessToken;
        return response()->json([
            'message' => 'User logged in successfully',
            'user' => [
                'data' => auth()->user(),
                'token' => $token,
            ],
        ], 200);
    }

    public function logout()
    {
        auth()->user()->token()->revoke();
        return response()->json([
            'message' => 'User logged out successfully',
        ], 200);
    }
}
