<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;

class CategoryController extends Controller
{
    //
    public function create()
    {
        try {
            Category::create([
                'name' => \request()->get('name'),
                'slug' => \request()->get('slug'),
            ]);
            return response()->json([
                'message' => 'Category created successfully',
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Error creating category',
                'error' => $e->getMessage(),
            ], 500);
        }
    }

    public function list()
    {
        try {
            $categories = Category::paginate(10);
            return response()->json([
                'message' => 'Categories fetched successfully',
                'data' => $categories,
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Error fetching categories',
                'error' => $e->getMessage(),
            ], 500);
        }
    }

    public function listAll()
    {
        try {
            $categories = Category::all();
            return response()->json([
                'message' => 'Categories fetched successfully',
                'data' => $categories,
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Error fetching categories',
                'error' => $e->getMessage(),
            ], 500);
        }
    }

    public function del($id)
    {
        try {
            $category = Category::find($id);
            $category->delete();
            return response()->json([
                'message' => 'Category deleted successfully',
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Error deleting category',
                'error' => $e->getMessage(),
            ], 500);
        }
    }

    public function getOne($id)
    {
        try {
            $category = Category::find($id);
            return response()->json($category, 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Error fetching category',
                'error' => $e->getMessage(),
            ], 500);
        }
    }

    public function update($id)
    {
        try {
            $category = Category::find($id);
            $category->update([
                'name' => \request()->get('name'),
                'slug' => \request()->get('slug'),
            ]);
            return response()->json([
                'message' => 'Category updated successfully',
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Error updating category',
                'error' => $e->getMessage(),
            ], 500);
        }
    }
}
