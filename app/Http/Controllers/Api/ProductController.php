<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Image;
use App\Models\Product;


class ProductController extends Controller
{
    //
    public function index()
    {
        try {
            $products = Product::with('image','category')->paginate(10);
            return response()->json([
                'message' => 'Product fetched successfully',
                'data' => $products,
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Error fetching product',
                'error' => $e->getMessage(),
            ], 500);
        }
    }

    public function create()
    {
        try {
            Product::create([
                'name' => \request()->get('name'),
                'description' => \request()->get('description'),
                'content' => \request()->get('content'),
                'category_id' =>request()->get('category_id'),
                'price' => \request()->get('price'),
            ]);
            if (\request()->hasFile('image')) {
                $image = \request()->file('image');
                $image->move(public_path('images'), $image->getClientOriginalName());
                $product = Product::latest()->first();
                Image::create([
                    'product_id' => $product->id,
                    'path' => url('/images/' . $image->getClientOriginalName()),
                ]);
            }
            return response()->json([
                'message' => 'Product created successfully',
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Error creating product',
                'error' => $e->getMessage(),
            ], 500);
        }
    }
}
