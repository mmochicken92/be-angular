<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1/auth')->group(function () {
    Route::post('/login', [\App\Http\Controllers\Api\AuthController::class, 'login']);
    Route::post('/logout', [\App\Http\Controllers\Api\AuthController::class, 'logout'])->middleware('auth:api');
});

Route::group(['prefix' => 'v1', 'middleware' => 'auth:api'], function () {
    Route::prefix('categories')->group(function () {
        Route::post('/create', [\App\Http\Controllers\Api\CategoryController::class, 'create']);
        Route::get('/', [\App\Http\Controllers\Api\CategoryController::class, 'list']);
        Route::put('/{id}', [\App\Http\Controllers\Api\CategoryController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\CategoryController::class, 'del']);
        Route::get('/list-all', [\App\Http\Controllers\Api\CategoryController::class, 'listAll']);
        Route::get('/{id}', [\App\Http\Controllers\Api\CategoryController::class, 'getOne']);
    });

    Route::prefix('products')->group(function () {
        //::get('/', 'Api\ProductController@index');
        Route::post('/create', [\App\Http\Controllers\Api\ProductController::class, 'create']);
        Route::get('/', [\App\Http\Controllers\Api\ProductController::class, 'index']);
        //Route::get('/{id}', 'Api\ProductController@show');
        Route::put('/{id}', [\App\Http\Controllers\Api\ProductController::class, 'update']);
        Route::delete('/{id}', [\App\Http\Controllers\Api\ProductController::class, 'del']);
        //Route::get('/{id}', [\App\Http\Controllers\Api\ProductController::class, 'getOne']);
    });
});


